package com.groundgurus.day2.exercises.supermarket;

import lombok.AllArgsConstructor;
import lombok.Builder;

@Builder
@AllArgsConstructor
public class Customer {
    String name;
    double cash;
    Cart cart;
    
    public void checkout() {
        // compute how much the customer will pay
        double total = 0.0;
        for (Item item : cart.items) {
            if (item != null) {
                total += item.price;
            }
        }

        double change = cash - total;
        System.out.println("Change: " + change);

        cash = change;
    }
}
