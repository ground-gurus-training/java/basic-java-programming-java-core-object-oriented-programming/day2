package com.groundgurus.day2.shop;

public class ShopApp {

    public static void main(String[] args) {
        Shop redRibbon = new Shop("Red Ribbon", new String[]{"Chocolate Cake", "Vanilla Cake"});
        
        redRibbon.printDetails();
    }
}
