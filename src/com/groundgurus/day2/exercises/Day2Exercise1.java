package com.groundgurus.day2.exercises;

import java.util.Scanner;

public class Day2Exercise1 {

    public static void main(String[] args) {
        int numbers[] = {5, 6, 4, 12, 10};
        
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Enter a value: ");
        int value = scanner.nextInt();
        
        int index = -1;

        // check the matching value in the array and get the index
        for (int i = 0; i < numbers.length; i++) {
            int num = numbers[i];

            if (value == num) {
                index = i;
                break;
            }
        }

        System.out.println(value + " is in the index " + index);

        scanner.close();
    }
}
