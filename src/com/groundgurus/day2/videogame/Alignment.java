package com.groundgurus.day2.videogame;


public enum Alignment {
    GOOD, NEUTRAL, EVIL
}
