package com.groundgurus.day2.exercises.supermarket;

public class SupermarketApp {

    public static void main(String[] args) {
        // cart of the customer
        Cart customerCart = new Cart();

        // represents the customer
//        Customer pedro = new Customer("Pedro Penduko", 500.0, customerCart);
        Customer pedro = Customer.builder()
                .name("Pedro Penduko")
                .cash(500.0)
                .cart(customerCart)
                .build();

        // items bought by the customer
        Item eggs = new Item("Eggs", 90.0, 12);
        Item butter = new Item("Butter", 20.0, 1);
        Item bread = new Item("Pack of bread", 40.0, 1);
        
        // add items to the cart
        customerCart.addItem(eggs);
        customerCart.addItem(butter);
        customerCart.addItem(bread);
        
        // customer will checkout
        pedro.checkout();
    }
}
