package com.groundgurus.day2.videogame;

public class VideoGameCharacter {

    public String name;
    public Alignment alignment;

    public void printDetails() {
        System.out.println("Name: " + this.name);
        System.out.println("Alignment: " + this.alignment);
    }

}
