package com.groundgurus.day2;

import java.util.Random;

public class ArrayExample {

    public static void main(String[] args) {
        // declaration and initalization of array in a single line
        // int numbers[] = {10, 20, 30, 40, 50};

        // initialization of array in a separate line
        // numbers = new int[]{10, 20, 30, 40, 50};

        // declaration of array with a fixed size
        int numbers[] = new int[100];
        
        //numbers[0] = 45;

        Random random = new Random();
        
        // set random values to each element
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = random.nextInt(50) + 1;
        }
        
        // print out the values of each element
        for (int number : numbers) {
            System.out.println("random number = " + number);
        }

        //System.out.println("There are " + numbers.length + " elements");
    }
}
