package com.groundgurus.day2.shop;

public class Shop {

    String name;
    String[] items;

    public Shop(String name, String[] items) {
        this.name = name;
        this.items = items;
    }

    public void printDetails() {
        System.out.println("Name: " + name);
        System.out.println("Items: ");
        for (String item : items) {
            System.out.println(item);
        }
    }
}
